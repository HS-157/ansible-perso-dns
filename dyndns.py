#!/usr/bin/python3
import argparse
from urllib.request import urlopen
from ansible_collections.community.general.plugins.module_utils.gandi_livedns_api import (
    GandiLiveDNSAPI,
)


def get_args():
    parser = argparse.ArgumentParser(description="Dyndns for Gandi")

    parser.add_argument("--api_key", help="Key for Gandi API", dest="api_key")

    parser.add_argument("--record", help="Subdomain", dest="record")

    parser.add_argument("--domain", help="Domain", dest="domain")

    return parser.parse_args()


args = get_args()

records = {
    "A": urlopen("https://ipv4.lafibre.info/ip.php").read().decode(),
    "AAAA": urlopen("https://ipv6.lafibre.info/ip.php").read().decode(),
}


class foo:
    def __init__(self, record, type_r, ttl, value, domain, api_key):
        self.params = {}
        self.params["type"] = type_r
        self.params["ttl"] = ttl
        self.params["record"] = record
        self.params["values"] = [value]
        self.params["domain"] = domain
        self.params["api_key"] = api_key
        self.tmpdir = "/tmp/gandi"
        self.check_mode = False


for type_r, value in records.items():
    print("%s : %s" % (type_r, value))

    f = foo(
        record=args.record,
        type_r=type_r,
        ttl=300,
        value=value,
        domain=args.domain,
        api_key=args.api_key,
    )

    gandi = GandiLiveDNSAPI(f)
    result = gandi.ensure_dns_record(
        f.params["record"],
        f.params["type"],
        f.params["ttl"],
        f.params["values"],
        f.params["domain"],
    )
